// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0

package sqlc

import (
	"time"
)

type TaskUpdate struct {
	ID        int64
	CreatedAt time.Time
	TaskID    string
	Payload   []byte
}
